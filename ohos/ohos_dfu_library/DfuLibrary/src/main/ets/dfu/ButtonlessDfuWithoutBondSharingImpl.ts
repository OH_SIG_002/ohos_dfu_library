/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ButtonlessDfuImpl } from './ButtonlessDfuImpl'
import { SecureDfuImpl } from './SecureDfuImpl'
import { BaseDfuImpl } from './BaseDfuImpl'
import ble from '@ohos.bluetooth.ble';
import hilog from '@ohos.hilog';

export class ButtonlessDfuWithoutBondSharingImpl extends ButtonlessDfuImpl {
  /** The UUID of the Secure DFU service from SDK 12. */
  static DEFAULT_BUTTONLESS_DFU_SERVICE_UUID = SecureDfuImpl.DEFAULT_DFU_SERVICE_UUID;
  /** The UUID of the Secure Buttonless DFU characteristic without bond sharing from SDK 13. */
  static DEFAULT_BUTTONLESS_DFU_UUID = '8EC90003-F315-4F60-9FB8-838830DAEA50';

  static BUTTONLESS_DFU_SERVICE_UUID: string = this.DEFAULT_BUTTONLESS_DFU_SERVICE_UUID;
  static BUTTONLESS_DFU_UUID: string = this.DEFAULT_BUTTONLESS_DFU_UUID;

  private mButtonlessDfuCharacteristic: ble.BLECharacteristic;

  public async isClientCompatible(gattServiceList: Array<ble.GattService>): Promise<boolean> {
    hilog.info(this.DOMAIN, this.TAG, 'ButtonlessDfuWithoutBondSharingImpl isClientCompatible');
    //TODO: find service with spec UUID
    for (const gsItem of gattServiceList) {
      hilog.info(this.DOMAIN, this.TAG, `gsItem.serviceUuid: ${gsItem.serviceUuid}, BLDWOB: ${ButtonlessDfuWithoutBondSharingImpl.BUTTONLESS_DFU_SERVICE_UUID}`);
      if (gsItem.serviceUuid === ButtonlessDfuWithoutBondSharingImpl.BUTTONLESS_DFU_SERVICE_UUID) {
        for (const gcItem of gsItem.characteristics) {
          hilog.info(this.DOMAIN, this.TAG, `gcItem.characteristicUuid: ${gcItem.characteristicUuid}, BLDWOB: ${ButtonlessDfuWithoutBondSharingImpl.BUTTONLESS_DFU_UUID}`);
          if (gcItem.characteristicUuid === ButtonlessDfuWithoutBondSharingImpl.BUTTONLESS_DFU_UUID) {
            for (const gdItem of gcItem.descriptors) {
              hilog.info(this.DOMAIN, this.TAG, `gsItem.serviceUuid: ${gdItem.descriptorUuid}, BLDWOB: ${ButtonlessDfuWithoutBondSharingImpl.CLIENT_CHARACTERISTIC_CONFIG}`);
              if (gdItem.descriptorUuid === ButtonlessDfuWithoutBondSharingImpl.CLIENT_CHARACTERISTIC_CONFIG.toUpperCase()) {
                this.mButtonlessDfuCharacteristic = gcItem;
                hilog.info(this.DOMAIN, this.TAG, 'Is ButtonlessDfuWithoutBondSharingImpl Client');
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  protected getResponseType(): number {
    return BaseDfuImpl.INDICATIONS;
  }

  protected getButtonlessDfuCharacteristic(): ble.BLECharacteristic {
    return this.mButtonlessDfuCharacteristic;
  }

  protected getDfuServiceUUID(): string {
    return SecureDfuImpl.DFU_SERVICE_UUID;
  }

  public shouldScanForBootloader(): boolean {
    return true;
  }

  public async performDfu() {
    hilog.info(this.DOMAIN, this.TAG, "Buttonless service without bond sharing found -> SDK 13 or newer");
    if (this.isBonded()) {
      hilog.warn(this.DOMAIN, this.TAG,
        "Device is paired! Use Buttonless DFU with Bond Sharing instead (SDK 14 or newer)");
    }

    super.performDfu();
  }
}