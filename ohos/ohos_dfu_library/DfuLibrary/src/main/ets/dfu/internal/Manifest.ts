/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export interface Manifest {
  'application': FileInfo,
  'bootloader': FileInfo,
  'softdevice': FileInfo,
  'softdeviceBootloader': SoftDeviceBootloaderFileInfo,
}

export interface SoftDeviceBootloaderFileInfo extends FileInfo {
  'bl_size': number;
  'sd_size': number;
}

export interface FileInfo {
  'bin_file': string,
  'dat_file': string,
}

export interface ManifestFile {
  'manifest': Manifest,
}

//TODO： delete
// export class Manifest {
//   private application: FileInfo;
//   private bootloader: FileInfo;
//   private softdevice: FileInfo;
//   private softdeviceBootloader: SoftDeviceBootloaderFileInfo;
//
//   private bootloaderApplication: FileInfo;
//   private softdeviceApplication: FileInfo;
//   private softdeviceBootloaderApplication: FileInfo;
//
//   public getApplicationInfo() {
//     if (this.application != null)
//       return this.application;
//     // The other parts will be sent together with application, so they may be returned here.
//     if (this.softdeviceApplication != null)
//       return this.softdeviceApplication;
//     if (this.bootloaderApplication != null)
//       return this.bootloaderApplication;
//     return this.softdeviceBootloaderApplication;
//   }
//
//   public getBootloaderInfo(): FileInfo {
//     return this.bootloader;
//   }
//
//   public getSoftdeviceInfo(): FileInfo {
//     return this.softdevice;
//   }
//
//   public getSoftdeviceBootloaderInfo(): SoftDeviceBootloaderFileInfo {
//     return this.softdeviceBootloader;
//   }
//
//   public isSecureDfuRequired(): boolean {
//     // Legacy DFU requires sending firmware type together with Start DFU command.
//     // The following options were not supported by the legacy bootloader,
//     // but in some implementations they are supported in Secure DFU.
//     // In Secure DFU the fw type is provided in the Init packet.
//     return this.bootloaderApplication != null ||
//       this.softdeviceApplication != null ||
//       this.softdeviceBootloaderApplication != null;
//   }
// }