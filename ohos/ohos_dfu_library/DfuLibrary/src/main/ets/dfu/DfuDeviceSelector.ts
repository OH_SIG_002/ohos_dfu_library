/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ble from '@ohos.bluetooth.ble';

export abstract class DfuDeviceSelector {

  public getScanFilters(dfuServiceUuid: string): Array<ble.ScanFilter> {
    let filters: Array<ble.ScanFilter> = new Array<ble.ScanFilter>();
    let scanFilter: ble.ScanFilter = {
      serviceUuid:dfuServiceUuid
    };
    let scanFilterAll: ble.ScanFilter = {};
    filters.push(scanFilter);
    filters.push(scanFilterAll);
    return filters;
  }


  public abstract matches(device: ble.ScanResult, rssi: number, scanRecord: Uint8Array,
    originalAddress: string, incrementedAddress: string, oriname: string): boolean;
}