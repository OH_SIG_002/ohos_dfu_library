/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export interface DfuLogListener {
  /**
   * Method called when a log event was sent from the DFU service.
   *
   * @param deviceAddress the target device address
   * @param level the log level, one of:
   * 		<ul>
   * 		    <li>{@link DfuBaseService#LOG_LEVEL_DEBUG}</li>
   * 		    <li>{@link DfuBaseService#LOG_LEVEL_VERBOSE}</li>
   * 		    <li>{@link DfuBaseService#LOG_LEVEL_INFO}</li>
   * 		    <li>{@link DfuBaseService#LOG_LEVEL_APPLICATION}</li>
   * 		    <li>{@link DfuBaseService#LOG_LEVEL_WARNING}</li>
   * 		    <li>{@link DfuBaseService#LOG_LEVEL_ERROR}</li>
   * 		</ul>
   * @param message the log message
   */
  onLogEvent(deviceAddress: string, level: number, message: string);
  dispose();
}