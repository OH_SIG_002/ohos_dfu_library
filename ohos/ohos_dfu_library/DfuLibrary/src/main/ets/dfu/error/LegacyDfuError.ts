/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DfuBaseService } from '../DfuBaseService';

export class LegacyDfuError {
  // DFU status values
  // public static final int SUCCESS = 1; // that's not an error
  public static INVALID_STATE: number = 2;
  public static NOT_SUPPORTED: number = 3;
  public static DATA_SIZE_EXCEEDS_LIMIT: number = 4;
  public static CRC_ERROR: number = 5;
  public static OPERATION_FAILED: number = 6;

  public static parse(error: number): string {
  switch (error) {
    case DfuBaseService.ERROR_REMOTE_TYPE_LEGACY | LegacyDfuError.INVALID_STATE:
      return "INVALID STATE";
    case DfuBaseService.ERROR_REMOTE_TYPE_LEGACY | LegacyDfuError.NOT_SUPPORTED:
      return "NOT SUPPORTED";
    case DfuBaseService.ERROR_REMOTE_TYPE_LEGACY | LegacyDfuError.DATA_SIZE_EXCEEDS_LIMIT:
      return "DATA SIZE EXCEEDS LIMIT";
    case DfuBaseService.ERROR_REMOTE_TYPE_LEGACY | LegacyDfuError.CRC_ERROR:
      return "INVALID CRC ERROR";
    case DfuBaseService.ERROR_REMOTE_TYPE_LEGACY | LegacyDfuError.OPERATION_FAILED:
      return "OPERATION FAILED";
    default:
      return "UNKNOWN (" + error + ")";
}
}
}