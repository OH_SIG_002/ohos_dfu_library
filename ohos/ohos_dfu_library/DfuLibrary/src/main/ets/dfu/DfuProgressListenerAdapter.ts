/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DfuProgressListener } from "./DfuProgressListener"
import hilog from '@ohos.hilog';

export class DfuProgressListenerAdapter implements DfuProgressListener {
  private TAG: string = "DfuProgressListenerAdapter";
  private DOMAIN: number = 0x8632;

  public dispose(): void {
    hilog.info(this.DOMAIN, this.TAG, `dispose`);
  }

  public onDeviceConnecting(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDeviceConnecting: ${deviceAddress}`);
  }
  
  public onDeviceConnected(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDeviceConnected: ${deviceAddress}`);
  }
  
  
  public onDfuProcessStarting(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDfuProcessStarting: ${deviceAddress}`);
  }
  
  
  public onDfuProcessStarted(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDfuProcessStarted: ${deviceAddress}`);
  }
  
  
  public onEnablingDfuMode(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onEnablingDfuMode: ${deviceAddress}`);
  }
  
  
  public onProgressChanged(deviceAddress: string, percent: number,
    speed: number, avgSpeed: number, currentPart: number, partsTotal: number) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onProgressChanged: ${deviceAddress}` +
      `${percent},${speed},${avgSpeed},${currentPart},${partsTotal}`);
  }
  
  
  public onFirmwareValidating(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onFirmwareValidating: ${deviceAddress}`);
  }
  
  
  public onDeviceDisconnecting(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDeviceDisconnecting: ${deviceAddress}`);
  }
  
  
  public onDeviceDisconnected(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDeviceDisconnected: ${deviceAddress}`);
  }
  
  
  public onDfuCompleted(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDfuCompleted: ${deviceAddress}`);
  }
  
  
  public onDfuAborted(deviceAddress: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onDfuAborted: ${deviceAddress}`);
  }
  
  
  public onError(deviceAddress: string,
    error: number, errorType: number, message: string) {
    // empty default implementation
    hilog.info(this.DOMAIN, this.TAG, `onError: ${deviceAddress}` +
      `${error},${errorType},${message}`);
  }
}
