/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ButtonlessDfuImpl } from './ButtonlessDfuImpl'
import { DfuBaseService } from './DfuBaseService'
import { SecureDfuImpl } from './SecureDfuImpl'
import ble from '@ohos.bluetooth.ble';
import hilog from '@ohos.hilog';

export class ButtonlessDfuWithBondSharingImpl extends ButtonlessDfuImpl {
  /**
   * The UUID of the Secure DFU service from SDK 12.
   */
  static DEFAULT_BUTTONLESS_DFU_SERVICE_UUID: string = SecureDfuImpl.DEFAULT_DFU_SERVICE_UUID;
  /**
   * The UUID of the Secure Buttonless DFU characteristic with bond sharing from SDK 14 or newer.
   */
  static DEFAULT_BUTTONLESS_DFU_UUID: string = '8EC90004-F315-4F60-9FB8-838830DAEA50';

  static BUTTONLESS_DFU_SERVICE_UUID: string = ButtonlessDfuWithBondSharingImpl.DEFAULT_BUTTONLESS_DFU_SERVICE_UUID;
  static BUTTONLESS_DFU_UUID: string = ButtonlessDfuWithBondSharingImpl.DEFAULT_BUTTONLESS_DFU_UUID;

  private mButtonlessDfuCharacteristic: ble.BLECharacteristic;

  constructor(service: DfuBaseService) {
    super(service);
    this.TAG = 'ButtonlessDfuWithBondSharingImpl';
  }

  protected getDfuServiceUUID(): string {
    return SecureDfuImpl.DFU_SERVICE_UUID;
  }

  protected getButtonlessDfuCharacteristic(): ble.BLECharacteristic {
    return this.mButtonlessDfuCharacteristic;
  }

  public shouldScanForBootloader(): boolean {
    return false;
  }

  public async isClientCompatible(gattServiceList: Array<ble.GattService>): Promise<boolean> {
    //TODO: find service with spec UUID
    hilog.info(this.DOMAIN, this.TAG, 'ButtonlessDfuWithBondSharingImpl isClientCompatible');
    for (const gsItem of gattServiceList) {
      if (gsItem.serviceUuid === ButtonlessDfuWithBondSharingImpl.BUTTONLESS_DFU_SERVICE_UUID) {
        for (const gcItem of gsItem.characteristics) {
          if (gcItem.characteristicUuid === ButtonlessDfuWithBondSharingImpl.BUTTONLESS_DFU_UUID) {
            for (const gdItem of gcItem.descriptors) {
              if (gdItem.descriptorUuid === ButtonlessDfuWithBondSharingImpl.CLIENT_CHARACTERISTIC_CONFIG.toUpperCase()) {
                this.mButtonlessDfuCharacteristic = gcItem;
                hilog.info(this.DOMAIN, this.TAG, 'Is ButtonlessDfuWithBondSharingImpl Client');
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  public getResponseType(): number {
    return ButtonlessDfuWithBondSharingImpl.INDICATIONS;
  }

  public async performDfu() {
    if (!this.isBonded()) {

    }
    super.performDfu()
  }
}