# ohos_dfu_library

## Introduction

ohos_dfu_library provides Nordic BLE nRF Series SoC Over-The Air firmware upgrade interface。

This project base on OpenHarmony, refer to [Android-DFU-Library]([GitHub - NordicSemiconductor/Android-DFU-Library: Device Firmware Update library and Android app](https://github.com/NordicSemiconductor/Android-DFU-Library)) develop the SDK。

## How to Install

1.Install

```
ohpm install @ohos/ohos_dfu_library
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony ohpm usage](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。




## How to Usage

1.Example

```typescript
import { DfuServiceInitiator, DfuServiceController,
  DfuServiceListenerHelper, DfuProgressListener } from '@ohos/ohos_dfu_library'

let starter: DfuServiceInitiator = new DfuServiceInitiator(mac);
hilog.info(DOMAIN, TAG, `DfuServiceInitiator`);
starter.setZipUri(this.fileName);

let progressListener: ProgressListener = new ProgressListener();
progressListener.bootloaderCB = this.bootloaderCB;
progressListener.dfuInitializedCB = this.dfuInitializedCB;
progressListener.uploadingCB = this.uploadingCB;
progressListener.completeCB = this.completeCB;
progressListener.dfuAbortedCB = this.abortCB;
progressListener.deviceConnectedCB = this.abortCB;
DfuServiceListenerHelper.registerProgressListener(progressListener);
this.dfuContorller = starter.start();
```



## Available APIs

DFU Library provides 4 APIs：

1. DfuServiceInitiator：create dfu service，set zip file，start OTA

   ```typescript
   // construct dfu service，set remote device mac；
   constructor(deviceAddress: string)；
   // set zip path（get filepath from filepicker）
   public setZipUri(uri: string): DfuServiceInitiator
   // start upload
   public start(): DfuServiceController
   ```

   **Notice**：other APIs are not test yet;

   

2. DfuServiceController：the controller of dfu service，just support ‘abort’ function

   ```typescript
   // stop upload，use emitter to notice dfu serivce，after abort, application can receive message from DfuProgressListener
   public abort()
   ```

   

3. DfuServiceListenerHelper：register DfuProgressListener

   ```typescript
   // register DfuProgressListener
   public static registerProgressListener(listener: DfuProgressListener)
   ```

   

4. DfuProgressListener：callback interface，develop should implement DfuProgressListener

   ```
   export interface DfuProgressListener {
     dispose();
     /**
      * Method called when the DFU service started connecting with the DFU target.
      *
      * @param deviceAddress the target device address.
      */
     onDeviceConnecting(deviceAddress: string);
   
     /**
      * Method called when the service has successfully connected, discovered services and found
      * DFU service on the DFU target.
      *
      * @param deviceAddress the target device address.
      */
     onDeviceConnected(deviceAddress: string);
   
     /**
      * Method called when the DFU process is starting. This includes reading the DFU Version
      * characteristic, sending DFU_START command as well as the Init packet, if set.
      *
      * @param deviceAddress the target device address.
      */
     onDfuProcessStarting(deviceAddress: string);
   
     /**
      * Method called when the DFU process was started and bytes about to be sent.
      *
      * @param deviceAddress the target device address
      */
     onDfuProcessStarted(deviceAddress: string);
   
     /**
      * Method called when the service discovered that the DFU target is in the application mode
      * and must be switched to DFU mode. The switch command will be sent and the DFU process
      * should start again. There will be no {@link #onDeviceDisconnected(String)} event following
      * this call.
      *
      * @param deviceAddress the target device address.
      */
     onEnablingDfuMode(deviceAddress: string);
   
     /**
      * Method called during uploading the firmware. It will not be called twice with the same
      * value of percent, however, in case of small firmware files, some values may be omitted.
      *
      * @param deviceAddress the target device address.
      * @param percent       the current status of upload (0-99).
      * @param speed         the current speed in bytes per millisecond.
      * @param avgSpeed      the average speed in bytes per millisecond
      * @param currentPart   the number pf part being sent. In case the ZIP file contains a Soft Device
      *                      and/or a Bootloader together with the application the SD+BL are sent as part 1,
      *                      then the service starts again and send the application as part 2.
      * @param partsTotal    total number of parts.
      */
     onProgressChanged(deviceAddress: string, percent: number, speed: number, avgSpeed: number,
       currentPart: number, partsTotal: number);
   
     onFirmwareValidating(deviceAddress: string);
   
     onDeviceDisconnecting(deviceAddress: string);
   
     onDeviceDisconnected(deviceAddress: string);
   
     onDfuCompleted(deviceAddress: string);
   
     onDfuAborted(deviceAddress: string);
   
     onError(deviceAddress: string, error: number, errorType: number, message: string);
   }
   ```



## Constraints
This project has been verified in the following versions：

-  DevEco：Build Version: 5.0.3.910, built on November 1, 2024
-  OS：HOS 5.0.0.49，HOS next，api版本5.0.2 （14） Canary1
-  Products：P70 + MLD HA20 + PLANET 3 PLUS


## Structure

```
|---- ohos_dfu_library
|     |---- AppScrope  # Sample App
|     |---- entry  # Sample App
|     |---- DfuLibrary  # Library Code
|           |---- src/main/ets  # Har Code
|                |---- dfu/   # Har Code
|            |---- index.ets          # Interface File
|            |---- *.json5      # Configuration File
|     |---- README.md  # Readme
|     |---- CHANGELOG.md  # ChangeLog
```

## How to Contribute

Any problem and requirement can submit an [Issue](https://gitee.com/openharmony-sig/ohos_dfu_library/issues) and [PR](https://gitee.com/openharmony-sig/ohos_dfu_library/pulls).


## License

This project is licensed under [BSD License](https://gitee.com/openharmony-sig/ohos_dfu_library/blob/master/LICENSE).
